# Render toy rmarkdown example

```{bash}
singularity exec dpet873.sif Rscript -e "rmarkdown::render('examples/toy1.Rmd')"

```
