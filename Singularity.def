BootStrap: docker 
From: ubuntu:20.04 

%labels   
  Name dpet873
  Version 0.1
  Maintainer DPET 873
  R_Version 4.2.1-2
  BIOC 3.15
  samtools 1.16.1
  bcftools 1.16


%environment   
  export SHELL=/bin/bash   
  export LC_ALL=en_US.UTF-8   
  export LANG=en_US.UTF-8   
  export LANGUAGE=en_US.UTF-8  
  export TZ=UTC
  export PATH=${PATH}


%post

  # Set software versions
  export R_VERSION=4.2.1
  export CRAN_VERSION=4.2.1-2.2004.0
  export BIOC_VERSION=3.15
  export BCFTOOLS_VERSION=1.16
  export SAMTOOLS_VERSION=1.16.1
  
  export DEBIAN_FRONTEND=noninteractive 
  export WGET="wget --progress=bar:force:noscroll --inet4-only"

  apt-get update
  apt-get install -y --no-install-recommends \
    locales \
    python3-pip \
    python3-dev \
    python3-venv \
    runit-systemd \
    tzdata \
    apt-utils \
    wget \
    curl \
    gnupg2 \
    ca-certificates \
    tmux \
    tree \
    less \
    git \
    make \
    gcc \
    build-essential \
    zlib1g-dev \
    libncurses5-dev \
    libgdbm-dev \
    libnss3-dev \
    libssl-dev \
    libsqlite3-dev \
    libreadline-dev \
    libffi-dev \
    libbz2-dev \
    liblzma-dev \
    lzma \
    lzma-dev \
    tk-dev


  locale-gen en_US.UTF-8
  
  # Set time 
  ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
  dpkg-reconfigure --frontend noninteractive  tzdata


  # Install R   
  echo "deb http://cran.r-project.org/bin/linux/ubuntu focal-cran40/" > \
    /etc/apt/sources.list.d/r.list   
  apt-key adv --keyserver keyserver.ubuntu.com \
    --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

  apt-get update
  apt-get install -y --no-install-recommends -t focal-cran40 \
    r-base-core=${CRAN_VERSION}* \
    r-base-dev=${CRAN_VERSION}* \
    r-base-html=${CRAN_VERSION}* \
    r-doc-html=${CRAN_VERSION}* \
    libcurl4-openssl-dev \
    libssl-dev \
    libxml2-dev \
    libcairo2-dev \
    libxt-dev \
    libxaw7-dev \
    libxaw7 \
    libopenblas-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libgit2-dev \
    libgsl-dev \
    libcairo2-dev \
    libpng-dev \
    libfreetype6-dev \
    libavfilter-dev \
    libpng-dev \
    libtiff-dev \
    libtiffxx5 \
    libtiff5-dev \
    libjpeg-dev \
    libdeflate-dev \
    libjbig-dev \
    libglu1-mesa-dev \
    libopenmpi-dev \
    libhiredis-dev \
    libhdf5-dev \
    libfftw3-dev \
    libudunits2-dev \
    libgdal-dev \
    libmpfr-dev \
    libgeos-dev \
    jags \
    cmake \
    cargo \
    libmagick++-dev \
    libgmp-dev \
    default-jdk \
    default-jre \
    pandoc \
    pandoc-citeproc-preamble \
    pandoc-citeproc \
    pandoc-data \
    pandoc-sidenote \
    texlive \
    texlive-fonts-recommended \
    texlive-latex-extra \
    texlive-bibtex-extra \
    ttf-mscorefonts-installer \
    texlive-xetex \
    biber \
    lmodern \
    node-highlight.js \
    node-normalize.css \
    libjs-bootstrap \
    libjs-highlight.js \
    libjs-jquery \
    libjs-jquery-datatables \
    libjs-jquery-ui \
    libjs-mathjax \
    libjs-modernizr \
    libjs-prettify \
    fonts-font-awesome

    ## Install basic R packages

    R CMD javareconf

   
    # Install some core R packages
    Rscript -e "install.packages(c( \
      'codetools', 'tidyverse', 'gtsummary', 'knitr', 'rmarkdown',  'kableExtra' \
    ), dependencies = TRUE)"

    ## Install Bioconductor along with some BIOC dependencies for some CRAN packages
    Rscript -e "install.packages('BiocManager', dependencies=TRUE)"
    Rscript -e "BiocManager::install(version = '${BIOC_VERSION}')"

    ## Install BIOC packages
    Rscript -e "BiocManager::install(c( \
       'Gviz', 'rtracklayer', 'plyranges', 'qvalue', 'ShortRead', 'Biostrings' \
    ))"  

    # Clean up
    apt-get clean
    rm -rf /var/lib/apt/lists/*

    unset DEBIAN_FRONTEND
    
%apprun bcftools
  /scif/apps/bcftools/bin/bcftools
%appinstall bcftools
  ${WGET} https://github.com/samtools/bcftools/releases/download/${BCFTOOLS_VERSION}/bcftools-${BCFTOOLS_VERSION}.tar.bz2 && \
  tar -jxvf bcftools-${BCFTOOLS_VERSION}.tar.bz2 && \
  rm bcftools-${BCFTOOLS_VERSION}.tar.bz2 && \
  cd bcftools-${BCFTOOLS_VERSION} && \
  ./configure && \
  make && \
  ln -s /scif/apps/bcftools/bcftools-${BCFTOOLS_VERSION}/bcftools /scif/apps/bcftools/bin/bcftools

%apprun samtools
  /scif/apps/samtools/bin/samtools
%appinstall samtools
  ${WGET} https://github.com/samtools/samtools/releases/download/${SAMTOOLS_VERSION}/samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
  tar -jxf samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
  rm samtools-${SAMTOOLS_VERSION}.tar.bz2 && \
  cd samtools-${SAMTOOLS_VERSION} && \
  ./configure && \
  make && \
  ln -s /scif/apps/samtools/samtools-${SAMTOOLS_VERSION}/samtools /scif/apps/samtools/bin/samtools
