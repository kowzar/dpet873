#!/bin/bash
#
#SBATCH --job-name=dpetpreproc
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --time=08:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=20
#SBATCH --mem-per-cpu=1G
#SBATCH --output=dpetpreproc-job-%J.stdout
#SBATCH --error=dpetpreproc-job-%J.stderr


# singularity container image file
mysif='/pine/scr/o/w/owzar/dpet873/cnt/dpet873.sif'

# local and container paths to data directory
locdata=/pine/scr/o/w/owzar/dpet873/data/
cntdata=/mnt/data/
# local and container paths to reference file directory
locref=/pine/scr/o/w/owzar/dpet873/ref/
cntref=/mnt/ref/
# local and container paths to output directory
locproc=/pine/scr/o/w/owzar/dpet873/proc/
cntproc=/mnt/proc/




# Reference fasta file
myfasta='GRCh38_full_analysis_set_plus_decoy_hla.fa'

# input vcf file
myfullvcf='20201028_CCDG_14151_B01_GRM_WGS_2020-08-05_chr22.recalibrated_variants.vcf.gz'

# Sample id
myid=NA12766
# Input cram file
mycram=${myid}.final.cram
# Output bcf file
mybcf=${myid}-chr22.bcf
# Output bam file
mybam=${myid}.bam

# Check MD5 of input vcf
md5sum ${locdata}${myfullvcf}
# Check MD5 of input fasta
md5sum ${locref}${myfasta}
# Check MD5 of input cram
md5sum ${locdata}${mycram}


# Extract genotype calls for sample
singularity \
    exec \
    --no-home \
    --bind ${locdata}:${cntdata}:ro \
    --bind ${locproc}:${cntproc}:rw \
    --app bcftools \
    ${mysif} \
    bcftools view \
    --samples ${myid} \
    --output-file ${cntproc}${mybcf} \
    --output-type b \
    --threads ${SLURM_NTASKS_PER_NODE} \
    ${cntdata}${myfullvcf}

# Index bcf file
singularity \
    exec \
    --no-home \
    --bind ${locproc}:${cntproc}:rw \
    --app bcftools \
    ${mysif} \
    bcftools index \
    --threads ${SLURM_NTASKS_PER_NODE} \
    --csi \
    ${cntproc}${mybcf}


# Convert cram file to bam
singularity \
    exec \
    --no-home \
    --bind ${locdata}:${cntdata}:ro \
    --bind ${locproc}:${cntproc}:rw \
    --bind ${locref}:${cntref}:ro \
    --app samtools \
    ${mysif} \
    samtools view \
    --bam \
    --reference ${cntref}${myfasta} \
    --threads ${SLURM_NTASKS_PER_NODE} \
    --output ${cntproc}${mybam} \
    ${cntdata}${mycram}


SLURM_NTASKS_PER_NODE=1
# Index bam file
singularity \
    exec \
    --no-home \
    --bind ${locproc}:/mnt/proc/:rw \
    --app samtools \
    ${mysif} \
    samtools index \
    -b \
    -@ ${SLURM_NTASKS_PER_NODE} \
    ${cntproc}${mybam}


# Check MD5 of output files
md5sum ${locproc}${mybam}
md5sum ${locproc}${mybcf}
