#!/bin/bash
#
#SBATCH --job-name=pileup-gviz
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=16384
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=8
#SBATCH --output=pileup-gviz-%J.stdout
#SBATCH --error=pileup-gviz-%J.stderr


# singularity container image file
mysif='/pine/scr/o/w/owzar/dpet873/cnt/dpet873.sif'

# local and container paths to data directory
locproc=/pine/scr/o/w/owzar/dpet873/proc/
cntproc=/mnt/proc/
# local and container paths to reference file directory
locref=/pine/scr/o/w/owzar/dpet873/ref/
cntref=/mnt/ref/


singularity \
    exec \
    --bind ${locproc}:${cntproc}:rw \
    --bind ${locref}:${cntref}:ro \
    ${mysif} \
    Rscript -e \
    "rmarkdown::render('pileup-gviz.Rmd')"
