#!/bin/bash
#
#SBATCH --job-name=dpet873-toy
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=2048
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=dpet873-toy-%J.stdout
#SBATCH --error=dpet873-toy-%J.stderr

mysif='/pine/scr/o/w/owzar/dpet873/cnt/dpet873.sif'



singularity exec \
  ${mysif} \
  Rscript -e \
  "rmarkdown::render('toy1.Rmd')"
